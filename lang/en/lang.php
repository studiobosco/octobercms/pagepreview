<?php return [
  'plugin' => [
    'name' => 'Page Preview',
    'description' => 'Replaces the "Preview" button for static pages with a real time preview of a page.',
  ],
  'preview' => 'Preview',
  'refresh_preview_label' => 'Refresh',
  'refresh_preview_title' => 'Refresh preview',
  'close_preview_title' => 'Close preview',
  'view_page_label' => 'View',
  'view_page_title' => 'Open page in new tab',
  'resize_preview_handle_label' => 'Drag to change the preview size.',

  'settings' => [
    'label' => 'Seitenvorschau',
    'description' => 'Einstellungen für die Seitenvorschau',
    'use_for_static_pages' => 'Für Seiten aktivieren',
    'use_for_mail_templates' => 'Für Mail-Vorlagen aktivieren',
    'use_for_blog_posts' => 'Für Blog-Artikel aktivieren',
    'blog_post_page' => 'Page with blog post component',
  ],

  'permissions' => [
    'manage_settings' => 'Einstellungen verwalten',
  ],
];
